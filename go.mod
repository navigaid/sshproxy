module gitlab.com/navigaid/sshproxy

go 1.13

require (
	github.com/Bowery/prompt v0.0.0-20190419144237-972d0ceb96f5
	github.com/elazarl/goproxy v0.0.0-20190421051319-9d40249d3c2f
	github.com/elazarl/goproxy/ext v0.0.0-20190421051319-9d40249d3c2f // indirect
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	gopkg.in/yaml.v2 v2.2.2
)
