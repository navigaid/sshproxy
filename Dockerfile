FROM golang AS builder
WORKDIR /src
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 go build -o /bin/sshproxy

FROM alpine
COPY --from=builder /bin/sshproxy /bin/sshproxy
COPY --from=builder /src/sshproxy.yaml /bin/sshproxy.yaml
EXPOSE 8123
CMD ["sshproxy", "-no-host-check"]
