#!/bin/bash

# Simple script to build releases for various platforms

set -e

REPODIR="$(dirname $(readlink -f "$0"))"

go mod download

VERSION=$(perl -ne '/version\s+=\s+"([^"]+)/ && print "$1"' version.go)

if [ -z $VERSION ]; then
    echo "Failed to detect version"
    exit 1
fi

echo "Processing $VERSION"

mkdir -p $VERSION
rm -r $VERSION/*

export CGO_ENABLED=0

echo "Linux amd64"
export GOOS=linux GOARCH=amd64
go build github.com/navigaid/sshproxy
tar -cvzf $VERSION/sshproxy-${GOOS}-${GOARCH}-${VERSION}.tar.gz ./sshproxy{,.yaml}

echo "Linux 386"
export GOOS=linux GOARCH=386
go build github.com/navigaid/sshproxy
tar -cvzf $VERSION/sshproxy-${GOOS}-${GOARCH}-${VERSION}.tar.gz ./sshproxy{,.yaml}

echo "Linux arm64 v7"
export GOOS=linux GOARCH=arm64 GOARM=7
go build github.com/navigaid/sshproxy
tar -cvzf $VERSION/sshproxy-${GOOS}-${GOARCH}-${VERSION}.tar.gz ./sshproxy{,.yaml}

rm sshproxy

echo "Mac"
export GOOS=darwin GOARCH=amd64
go build github.com/navigaid/sshproxy
tar -cvzf $VERSION/sshproxy-mac-${VERSION}.tar.gz ./sshproxy{,.yaml}
rm sshproxy

echo "Windows 64bit"
export GOOS=windows GOARCH=amd64
go build github.com/navigaid/sshproxy
zip $VERSION/sshproxy-${GOOS}-${GOARCH}-${VERSION}.zip ./sshproxy{.exe,.yaml}
rm sshproxy.exe

echo "Windows 32bit"
export GOOS=windows GOARCH=386
go build github.com/navigaid/sshproxy
zip $VERSION/sshproxy-${GOOS}-${GOARCH}-${VERSION}.zip ./sshproxy{.exe,.yaml}
rm sshproxy.exe

echo ls -l $PWD/$VERSION | tee /dev/stderr | bash
